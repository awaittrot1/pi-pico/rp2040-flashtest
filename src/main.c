/*
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 * Copyright (c) 2024 awaittrot
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 *    disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <stdio.h>
#include <stdint.h>
#include "pico/stdlib.h"
#include "pico/bootrom.h"
#include "hardware/pll.h"
#include "hardware/clocks.h"
#include "hardware/flash.h"
#include "hardware/structs/ssi.h"
#include "hardware/structs/xip_ctrl.h"

void measure_freqs(void) {
    uint f_pll_sys = frequency_count_khz(CLOCKS_FC0_SRC_VALUE_PLL_SYS_CLKSRC_PRIMARY);
    uint f_pll_usb = frequency_count_khz(CLOCKS_FC0_SRC_VALUE_PLL_USB_CLKSRC_PRIMARY);
    uint f_rosc = frequency_count_khz(CLOCKS_FC0_SRC_VALUE_ROSC_CLKSRC);
    uint f_clk_sys = frequency_count_khz(CLOCKS_FC0_SRC_VALUE_CLK_SYS);
    uint f_clk_peri = frequency_count_khz(CLOCKS_FC0_SRC_VALUE_CLK_PERI);
    uint f_clk_usb = frequency_count_khz(CLOCKS_FC0_SRC_VALUE_CLK_USB);
    uint f_clk_adc = frequency_count_khz(CLOCKS_FC0_SRC_VALUE_CLK_ADC);
    uint f_clk_rtc = frequency_count_khz(CLOCKS_FC0_SRC_VALUE_CLK_RTC);

    printf("pll_sys  = %dkHz\n", f_pll_sys);
    printf("pll_usb  = %dkHz\n", f_pll_usb);
    printf("rosc     = %dkHz\n", f_rosc);
    printf("clk_sys  = %dkHz\n", f_clk_sys);
    printf("clk_peri = %dkHz\n", f_clk_peri);
    printf("clk_usb  = %dkHz\n", f_clk_usb);
    printf("clk_adc  = %dkHz\n", f_clk_adc);
    printf("clk_rtc  = %dkHz\n", f_clk_rtc);

    // Can't measure clk_ref / xosc as it is the ref
}

uint32_t *flash = (void *)XIP_NOCACHE_BASE;
uint8_t random_buf[256];
uint32_t xorshift_state = 1;
static ssi_hw_t *const ssi = (ssi_hw_t *) XIP_SSI_BASE;

uint32_t xorshift32(){
	/* Algorithm "xor" from p. 4 of Marsaglia, "Xorshift RNGs" */
	xorshift_state ^= xorshift_state << 13;
	xorshift_state ^= xorshift_state >> 17;
	xorshift_state ^= xorshift_state << 5;
	return xorshift_state;
}

bool wait_ssi_ready(){
    while(0 == (ssi->sr & SSI_SR_TFE_BITS));
    while(1 == (ssi->sr & SSI_SR_BUSY_BITS));
}

#define CMD_READ 0xeb
#define MODE_CONTINUOUS_READ 0xa0
#define WAIT_CYCLES 4

uint8_t read_sreg(uint8_t cmd){
    ssi->dr0 = cmd;
    ssi->dr0 = 0;
    wait_ssi_ready();
    cmd = ssi->dr0;
    return ssi->dr0;
}

void enter_qspi(){
    uint32_t reg;
    rom_connect_internal_flash_fn connect_internal_flash = (rom_connect_internal_flash_fn)rom_func_lookup_inline(ROM_FUNC_CONNECT_INTERNAL_FLASH);
    rom_flash_exit_xip_fn flash_exit_xip = (rom_flash_exit_xip_fn)rom_func_lookup_inline(ROM_FUNC_FLASH_EXIT_XIP);

    connect_internal_flash();
    flash_exit_xip();

    ssi->ssienr = 0;
    ssi->baudr = 2;
    ssi->rx_sample_dly = 1;

    /* program sregs */
    ssi->ctrlr0 =
            (7 << SSI_CTRLR0_DFS_32_LSB) |                              // 8 clocks per data frame
            (SSI_CTRLR0_TMOD_VALUE_TX_AND_RX << SSI_CTRLR0_TMOD_LSB);   // Both
    ssi->ssienr = 1;

    if(!(read_sreg(0x35) & 2)){
        /* write enable volatile status */
        ssi->dr0 = 0x50;
        wait_ssi_ready();
        reg = ssi->dr0;

        /* write status */
        ssi->dr0 = 0x01;
        ssi->dr0 = 0;
        ssi->dr0 = 2; /* QUAD ENABLE */
        wait_ssi_ready();
        reg = ssi->dr0;
        reg = ssi->dr0;
        reg = ssi->dr0;

        while(read_sreg(0x5) & 1);
    }

    /* dummy read */
    ssi->ssienr = 0;
    ssi->ctrlr0 =
            (SSI_CTRLR0_SPI_FRF_VALUE_QUAD << SSI_CTRLR0_SPI_FRF_LSB) |
            (31 << SSI_CTRLR0_DFS_32_LSB) |
            (SSI_CTRLR0_TMOD_VALUE_EEPROM_READ << SSI_CTRLR0_TMOD_LSB);
    ssi->ctrlr1 = 0;
    ssi->spi_ctrlr0 =
            (8 << SSI_SPI_CTRLR0_ADDR_L_LSB) |    // 24-bit address + 8-bit mode
            (2 << SSI_SPI_CTRLR0_INST_L_LSB) |    // 8-bit inst
            (WAIT_CYCLES << SSI_SPI_CTRLR0_WAIT_CYCLES_LSB) |
            (SSI_SPI_CTRLR0_TRANS_TYPE_VALUE_1C2A  // Send inst in SPI, address in Quad SPI
                    << SSI_SPI_CTRLR0_TRANS_TYPE_LSB);
    ssi->ssienr = 1;
    ssi->dr0 = CMD_READ;
    ssi->dr0 = MODE_CONTINUOUS_READ; /* addr=0, mode=continuous */
    wait_ssi_ready();

    /* configure xip in continuous read */
    ssi->ssienr = 0;
    ssi->spi_ctrlr0 =
            (MODE_CONTINUOUS_READ << SSI_SPI_CTRLR0_XIP_CMD_LSB) | // Standard 03h read
            (8 << SSI_SPI_CTRLR0_ADDR_L_LSB) |    // 24-bit address + 8-bit dummy
            (0 << SSI_SPI_CTRLR0_INST_L_LSB) |    // Send XIP_CMD after address
            (WAIT_CYCLES << SSI_SPI_CTRLR0_WAIT_CYCLES_LSB) |    //
            (SSI_SPI_CTRLR0_TRANS_TYPE_VALUE_2C2A  // Send address in Quad SPI
                    << SSI_SPI_CTRLR0_TRANS_TYPE_LSB);
    ssi->ssienr = 1;
}

void enter_serial(){
    rom_connect_internal_flash_fn connect_internal_flash = (rom_connect_internal_flash_fn)rom_func_lookup_inline(ROM_FUNC_CONNECT_INTERNAL_FLASH);
    rom_flash_exit_xip_fn flash_exit_xip = (rom_flash_exit_xip_fn)rom_func_lookup_inline(ROM_FUNC_FLASH_EXIT_XIP);

    connect_internal_flash();
    flash_exit_xip();

    /* Serial mode */
    ssi->ssienr = 0;
    ssi->baudr = 4;
    ssi->ctrlr0 =
            (SSI_CTRLR0_SPI_FRF_VALUE_STD << SSI_CTRLR0_SPI_FRF_LSB) |  // Standard 1-bit SPI serial frames
            (31 << SSI_CTRLR0_DFS_32_LSB) |                             // 32 clocks per data frame
            (SSI_CTRLR0_TMOD_VALUE_EEPROM_READ << SSI_CTRLR0_TMOD_LSB); // Send instr + addr, receive data
    ssi->spi_ctrlr0 =
            (0x03 << SSI_SPI_CTRLR0_XIP_CMD_LSB) | // Standard 03h read
            (2u << SSI_SPI_CTRLR0_INST_L_LSB) |    // 8-bit instruction prefix
            (6u << SSI_SPI_CTRLR0_ADDR_L_LSB) |    // 24-bit addressing for 03h commands
            (SSI_SPI_CTRLR0_TRANS_TYPE_VALUE_1C1A  // Command and address both in serial format
                    << SSI_SPI_CTRLR0_TRANS_TYPE_LSB);
    ssi->ssienr = 1;
}

void timed_flash_erase(uint32_t addr){
    uint32_t t0, t1;
    t0 = time_us_32();
    flash_range_erase(addr, FLASH_BLOCK_SIZE);
    t1 = time_us_32();
    printf("%08x erased %d us\n", addr, t1-t0);
}

void timed_flash_random(uint32_t addr){
    uint32_t t0, t1;
    uint32_t *ptr = (void *)random_buf;
    for(int i=0; i<sizeof(random_buf)/4; i++){
        ptr[i] = xorshift32();
    }
    t0 = time_us_32();
    flash_range_program(addr, random_buf, sizeof(random_buf));
    t1 = time_us_32();
    printf("%08x programmed %d us\n", addr, t1-t0);
}

bool timed_flash_verify(uint32_t bytes){
    uint32_t t0, t1;
    uint32_t *ptr = (void *)random_buf;
    xorshift_state = 1;
    t0 = time_us_32();
    for(int i=0; i<bytes/4; i++){
        if(xorshift32() != flash[i]) {
            printf("Verify failed at %08x\n", i);
            return false;
            break;
        }
    }
    t1 = time_us_32();
    printf("Verify finished %d us\n", t1-t0);
    return true;
}

int main(void){
    uint32_t sys_freq_khz = 125000;
    clock_configure(clk_peri,
                    0,
                    CLOCKS_CLK_PERI_CTRL_AUXSRC_VALUE_CLKSRC_PLL_USB,
                    48 * MHZ,
                    48 * MHZ);
    stdio_init_all();
#ifdef _PICO_STDIO_USB_H
    while(!stdio_usb_connected());
#endif
    measure_freqs();

    printf("PICO_FLASH_SIZE_BYTES = %d KiB\n", PICO_FLASH_SIZE_BYTES >> 10);

    enter_serial();
    if(!timed_flash_verify(PICO_FLASH_SIZE_BYTES)){
        printf("Programming flash for testing\n");
        xorshift_state = 1;
        for(uint32_t i=0; i<(PICO_FLASH_SIZE_BYTES / FLASH_BLOCK_SIZE); i++){
            timed_flash_erase(i * FLASH_BLOCK_SIZE);
        }
        for(uint32_t i=0; i<(PICO_FLASH_SIZE_BYTES / FLASH_PAGE_SIZE); i++){
            timed_flash_random(i * FLASH_PAGE_SIZE);
        }

        if(!timed_flash_verify(PICO_FLASH_SIZE_BYTES)){
            printf("Bad flash or incorrect PICO_FLASH_SIZE_BYTES specified?\n");
            return 0;
        }
    }

    enter_qspi();

    while(1){
        do {
            sys_freq_khz += 1000;
        } while(false == set_sys_clock_khz(sys_freq_khz, false));
        printf("sys_freq %d MHz\n", sys_freq_khz / 1000);
        if(!timed_flash_verify(PICO_FLASH_SIZE_BYTES)){
            break;
        }
    }
    printf("Test finished\n");
    while(1);
    return 0;
}
