# rp2040-flashtest

Test maximum flash reading speed on your rp2040 board.
Only QSPI flash memories compatible to w25q080 is supported for now.
Running this software erases entire regions of on-board flash memory.

## Building
Binary is built in a similar way to pico-examples.

Build dependencies:

* Target C compiler, binutils and libc on your system
* Host C++ compiler (required to build .uf2 binary format)
* python3, cmake, gnumake

```
$ git submodule update --init pico-sdk
$ cd pico-sdk
$ git submodule update --init lib/tinyusb
$ cd ../src
$ mkdir build
$ cmake -B build
$ make -C build
```

## Usage
Load firmware on your board and connect to USB serial console.

## Bugs
Currently there is no way to detect CPU instability caused by high sys\_clk.

## Support
If you have any questions please contact on Gitlab issue.
Response may not be available in timely manner so please be patient.

## Roadmap
Currently there is no roadmap defined.

## Contributing
Contributions are welcome on Gitlab issue, MR or by e-mail.
Please test your patches before sending.

## Authors and acknowledgment
Project is written by awaittrot.
Thanks to Raspberry Pi (Trading) Ltd. for providing pico-sdk and examples.

## License
See indivisual files.

## Project status
Working.
